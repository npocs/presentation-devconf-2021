---
title: Closing gaps in strong auth`:`
subtitle: FIDO2 device support
author: |
	| Norbert Pócs (npocs@redhat.com)
	| Daiki Ueno (dueno@redhat.com)
---

# What comes
- What is FIDO?
- How does it work?
- Password-less authentication
- Our project

# What is FIDO?
- Fast IDentity Online
- Enhances user authentication security
- 2FA/MFA/password-less

## Benefits of FIDO
- Stronger security for the users
- Stronger security for the server admins

# How FIDO works?
- Public-key cryptography
- Operations:
![Operations of FIDO2](fido2_operations.png)

# FIDO2 standard
- A successor of the previous FIDO standards (U2F/CTAP1)
- WebAuthn (JavaScript + REST) and CTAP2 (low-level transport)
- Algorithm agility: not only ES256
- User verification: PIN, biometrics, etc.

![](fido2_standard.png)

# Password-less authentication
- PII remains on the client side
- No password is sent over the internet
- Requires user presence and verification
![Password-less authentication](passwordless.png)

# The problems
- Requires full access to the devices
- User verification capable devices are rare in the market

# Flatpak
![Flatpak](problem_flatpak.png){width=60%}

# Our approach
- FIDO2 authenticators as a user service on the host
- Enable virtual password-less authenticator

# Implementation
- D-Bus
- Asynchronous API
- FD passing
- Fine grained access control through xdg-dbus-proxy
- Activation through systemd

# Demo: Using 2FA from Flatpak

\centering

[![](run.png){width=2cm}](fido2.webm)

# Demo: Emulating password-less auth with TPM2

\centering

[![](run.png){width=2cm}](fido2-tpm2.webm)

# Status and future

## Current status

- D-Bus proxy service, with libfido2 and TPM2 backends
- Client library
- Firefox integration is under review

## Future plans

- Configuration files
- Bring it to distributions: Debian/Fedora/RHEL
- More client adoption: SSH (OpenSSH and libssh), browsers (WebKit and Chromium)

# Summary

## FIDO2

- The standard: WebAuthn + CTAP2
- User verification with PIN, biometrics, etc.
- Password-less makes verification at the client side

## Our project

- Goal: increase adoption of FIDO2 clients
- Authenticator proxy based on D-Bus
- Device emulation for password-less authentication
